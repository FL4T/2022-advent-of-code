with open("data.txt", "r") as f:
    rucksacks = [line.rstrip() for line in f]
# item priority value dicts
low_pris = {}
high_pris = {}

# keys for dicts
low_keys = [chr(x) for x in range(97,123 )]
high_keys = [chr(x) for x in range(65,91)]

# populate dicts
val = 0
for key in low_keys:
    val +=1
    low_pris[key] = val

for key in high_keys:
    val += 1
    high_pris[key] = val

pri_sum = 0 # puzzle answer

# check group of 3 rucksacks for same item
for x in range(0,len(rucksacks),3):
    ruck1 = rucksacks[x]
    ruck2 = rucksacks[x+1]
    ruck3 = rucksacks[x+2]

    for item in ruck1:
        if item in ruck2 and item in ruck3:
            print(f"Badge is: {item}")

            if item in high_pris.keys():
                pri_sum += high_pris[item]
                print(f"Priority value: {high_pris[item]}")
                break

            elif item in low_pris.keys():
                pri_sum += low_pris[item]
                print(f"Priority value: {low_pris[item]}")
                break
    
print(f"Priority Sum: {pri_sum}")
