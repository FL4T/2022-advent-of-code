with open("data.txt", "r") as f:
    rucksacks = [line.rstrip() for line in f]

low_keys = [chr(x) for x in range(97,123 )]
high_keys = [chr(x) for x in range(65,91)]

low_pris = {}
high_pris = {}

val = 0
for key in low_keys:
    val +=1
    low_pris[key] = val

for key in high_keys:
    val += 1
    high_pris[key] = val

pri_sum = 0
for rucksack in rucksacks:
    print(f"Current SACK: {rucksack}")
    pocket_size = int(len(rucksack)/2)

    pocket1 = list(rucksack[:pocket_size])
    pocket2 = list(rucksack[pocket_size:])

    for item in pocket1:
        if item in pocket2:
            print(f"Item found in both pockets: {item}")

            if item in high_pris.keys():
                pri_sum += high_pris[item]
                print(f"Priority value: {high_pris[item]}")
                break

            elif item in low_pris.keys():
                pri_sum += low_pris[item]
                print(f"Priority value: {low_pris[item]}")
                break

print(f"Priority Sum: {pri_sum}")