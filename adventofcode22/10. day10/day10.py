with open("data.txt", "r") as f:
    instructions = [instr.rstrip() for instr in f]

x = 1
cycle_count = 0
cycle_x_vals = [1]

for instruction in instructions:
    instruction = instruction.split()
    if 'noop' in instruction:
        # start of cycle
        cycle_x_vals.append(x)
        cycle_count += 1

    else: # addx
        # start of cycle 1
        num = int(instruction[-1])
        cycle_count += 1 # end of cycle 1

        # start of cycle 2
        cycle_x_vals.append(x)
        cycle_count += 1 # end of cycle 2
        x = x + num
        cycle_x_vals.append(x)

sig_strs = [
    20 * cycle_x_vals[19],
    60 * cycle_x_vals[59],
    100 * cycle_x_vals[99],
    140 * cycle_x_vals[139],
    180 * cycle_x_vals[179],
    220 * cycle_x_vals[219]
]

print(f"Part 1 Answer: {sum(sig_strs)}")
    