with open("data.txt", "r") as f:
    instructions = [instr.rstrip() for instr in f]

# for part 2
def draw_pixel(new_x, cycle_num):
    sprite_pos = [new_x-1, new_x, new_x+1]
    if cycle_num >= 40:
        cycle_num = 0
        print('')
    if cycle_num in sprite_pos:
        print("#",end='')
    else:
        print('.',end='')
    return cycle_num

x = 1
cycle_count = 0
cycle_x_vals = [1]

for instruction in instructions:
    instruction = instruction.split()
    if 'noop' in instruction:
        # start of cycle
        cycle_x_vals.append(x)
        cycle_count = draw_pixel(x, cycle_count)
        cycle_count += 1

    else: # addx
        num = int(instruction[-1])
        # start of cycle 1
        cycle_count = draw_pixel(x, cycle_count)
        cycle_count += 1 # end of cycle 1
        cycle_x_vals.append(x)
        
        # start of cycle 2
        cycle_count = draw_pixel(x, cycle_count)
        cycle_count += 1 # end of cycle 2
        x = x + num
        cycle_x_vals.append(x)

sig_strs = [
    20 * cycle_x_vals[19],
    60 * cycle_x_vals[59],
    100 * cycle_x_vals[99],
    140 * cycle_x_vals[139],
    180 * cycle_x_vals[179],
    220 * cycle_x_vals[219]
]

print(f"Part 1 Answer: {sum(sig_strs)}")