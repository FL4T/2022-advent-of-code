import collections

with open("test.txt", "r") as f:
    data = [line.rstrip() for line in f]
# print(data)

def bfs(grid, start):
    queue = collections.deque([[start]])
    seen = set([start])
    iterations = 0
    while queue:
        path = queue.popleft()
        x, y = path[-1]
        try:
            cur_val = ord(grid[x][y])

        except:
            continue
        
        # path found
        if grid[y][x] == END:
            return path

        # check right, left, up, down from current position
        for x2, y2 in ((x+1,y), (x-1,y), (x,y+1), (x,y-1)):
            iterations += 1
            if 0 <= x2 < width and 0 <= y2 < height:
                if abs(ord(grid[y2][x2]) - cur_val == 1) or ord(grid[y2][x2]) <= cur_val and (x2, y2) not in seen:
                # if grid[y2][x2] <= grid[x][y] and (x2, y2) not in seen:
                    queue.append(path + [(x2, y2)])
                    seen.add((x2, y2))

# build 2D grid of topology
board = [list(row) for row in data]
print(board)
width = len(board[0])
height = len(board)
for row in range(len(board)):
    print('')
    for col in range(len(board[0])):
        print(board[row][col], end=' ')
        if board[row][col] == "S":
            START = (row, col)
            board[row][col] = "a"
        elif board[row][col] == "E":
            END = (row, col)
            board[row][col] = "z"

print(f"\n\nafter start/end found:")
for row in range(len(board)):
    print('')
    for col in range(len(board[0])):
        print(board[row][col], end=' ')

print(f"\nStart: {START}")
print(f"End: {END}")

path = bfs(board, START)
print(path)
