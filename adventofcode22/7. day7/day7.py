class Folder:
    def __init__(self, name, parent_dir=None):
        self.parent = parent_dir
        self.name = name
        self.size = 0
        self.children = []
    
    def add_child(self, child):
        self.children.append(child)
    
dir_stack = []
cur_dir = None

with open("data.txt", "r") as f:
    cmd_stack = [line.rstrip() for line in f]
# print(cmd_stack)

root = Folder('/')
dir_stack.append(root)
cur_dir = root
cmd_stack.pop(0)

cmd_idx = 0
while len(cmd_stack) > 0:
    # change cur_dir to cur_dir's parent
    if 'cd ..' in cmd_stack[cmd_idx]:
        cur_dir = cur_dir.parent
        cmd_stack.pop(cmd_idx)
    
    # move to tgt dir by looping through children until tgt dir is found, then make it cur_dir
    elif '$ cd' in cmd_stack[cmd_idx]:
        split_cmd = cmd_stack[cmd_idx].split()
        tgt_dir = split_cmd[-1]

        for child in cur_dir.children:
            if type(child) == Folder and child.name == tgt_dir:
                cur_dir = child
                break
        cmd_stack.pop(cmd_idx)

    # add files/dirs to current folder
    elif 'ls' in cmd_stack[cmd_idx]:
        while '$ cd' not in cmd_stack[cmd_idx]:
            cmd_stack.pop(cmd_idx)
            if len(cmd_stack) == 0:
                break
            
            # add new directory inside cur_dir
            elif 'dir' in cmd_stack[cmd_idx]:
                add_dir_name = cmd_stack[cmd_idx].split()
                dir_stack.append(Folder(add_dir_name[-1], cur_dir))
                cur_dir.children.append(dir_stack[-1])

            # exit loop to change directory
            elif '$ cd' in cmd_stack[cmd_idx]:
                break

            else: # add new file to cur_dir
                cur_dir.children.append(cmd_stack[cmd_idx].rsplit())
                if cur_dir.name == '/':
                    cur_dir.size += int(cur_dir.children[-1][0])
                else:
                    cur_dir.size += int(cur_dir.children[-1][0])
                    updating_dir_size = cur_dir
                    while updating_dir_size.name != '/':
                        updating_dir_size.parent.size += int(cur_dir.children[-1][0])
                        updating_dir_size = updating_dir_size.parent

# array of sizes for each directory
dir_sizes = [x.size for x in dir_stack]
part1_ans = 0
for size in dir_sizes:
    if size <= 100000:
        part1_ans += size
print(f"Part1: {part1_ans}")

# for part 2
TOTAL_SPACE = 70000000
NEEDED_SPACE = 30000000
root_size = dir_sizes[0]
free_space = TOTAL_SPACE - root_size
tgt_delete_size = NEEDED_SPACE - free_space

# find first directory that frees up enough space
dir_sizes.sort()
for size in dir_sizes:
    if free_space + size >= NEEDED_SPACE:
        print(f"dir size found: {size}")
        break
print(free_space)
