from copy import copy

move_list = []

with open("test2.txt", "r") as f:
    for line in f:
        move_list.append(line.rsplit())

class Knot:
    def __init__(self, id, next = None):
        self.id = id
        self.next = next
        self.position = [0, 0]
        self.prev_position = [0, 0]

class Rope:
    def __init__(self):
        self.head = None
        self.tail = None
        self.size = 0
        self.visited = [(0,0)]
    
    def add_knot(self, knot_id):
        new_knot = Knot(knot_id)
        self.size += 1
        # if knots exist, add new_knot to end of rope
        if self.tail != None:
            self.tail.next = new_knot
            self.tail = new_knot
        
        else: # make new_knot the head and tail
            self.head = new_knot
            self.tail = new_knot
    
    def print_knots(self):
        probe = self.head
        while probe != None:
            print(f"Probe: {probe.id} is at {probe.position} // prev_pos: {probe.prev_position}")
            probe = probe.next
        print("\n")

    def update_middle(self):
        pass

    def move_head(self, dir):
        self.dir = dir
        cur_knot = self.head
        cur_knot.prev_position = copy(cur_knot.position)
        if self.dir == "R":
            cur_knot.position[1] += 1
        
        elif self.dir == "L":
            cur_knot.position[1] -= 1
        
        elif self.dir == "U":
            cur_knot.position[0] += 1
        
        else: # "D"
            cur_knot.position[0] -= 1
        
        # check if knots need to move UP TO knot before tail
        while cur_knot.next != None:
            cur_knot.next.prev_position = copy(cur_knot.next.position)
            if cur_knot.next == self.tail:
                # check if cur_knot and next knot are on same ROW and 2 spaces apart
                if cur_knot.position[0] == cur_knot.next.position[0] and abs(cur_knot.position[1] - cur_knot.next.position[1]) > 1:
                    # set cur_knot position = cur_knot prev position and add coord to visited
                    cur_knot.next.position = cur_knot.prev_position
                    
                # check if head and tail are on same COL and 2 spaces apart
                elif cur_knot.position[1] == cur_knot.next.position[1] and abs(cur_knot.position[0] - cur_knot.next.position[0]) > 1:
                    # set cur_knot.next.position = prev_head and add coord to visited
                    cur_knot.next.position = cur_knot.prev_position
                
                # check if cur_knot.position and cur_knot.next.position are on dif ROW and COL and 2 spaces apart
                elif (cur_knot.position[0] != cur_knot.next.position[0] and cur_knot.position[1] != cur_knot.next.position[1]) and abs(cur_knot.position[0] - cur_knot.next.position[0]) > 1 or abs(cur_knot.position[1] - cur_knot.next.position[1]) > 1:
                    # set cur_knot.next.position = cur_knot.prev_position and add coord to visited
                    cur_knot.next.position = cur_knot.prev_position
                
                
                # # check if cur_knot and next knot are on same ROW and 2 spaces apart
                # if cur_knot.position[0] == self.tail.position[0] and abs(cur_knot.position[1] - self.tail.position[1]) > 1:
                #     # set cur_knot position = cur_knot prev position and add coord to visited
                #     self.tail.position = cur_knot.prev_position
                # # check if head and tail are on same COL and 2 spaces apart
                # elif cur_knot.position[1] == self.tail.position[1] and abs(cur_knot.position[0] - self.tail.position[0]) > 1:
                #     # set self.tail.position = prev_head and add coord to visited
                #     self.tail.position = cur_knot.prev_position
                # # check if cur_knot.position and self.tail.position are on dif ROW and COL and 2 spaces apart
                # elif (cur_knot.position[0] != self.tail.position[0] and cur_knot.position[1] != self.tail.position[1]) and abs(cur_knot.position[0] - self.tail.position[0]) > 1 or abs(cur_knot.position[1] - self.tail.position[1]) > 1:
                #     # set self.tail.position = cur_knot.prev_position and add coord to visited
                #     self.tail.position = cur_knot.prev_position

                self.visited.append(tuple(self.tail.position))

            else: # knots between before tail
                # check if cur_knot and next knot are on same ROW and 2 spaces apart
                if cur_knot.position[0] == cur_knot.next.position[0] and abs(cur_knot.position[1] - cur_knot.next.position[1]) > 1:
                    # set cur_knot position = cur_knot prev position and add coord to visited
                    cur_knot.next.position = cur_knot.prev_position
                    
                # check if head and tail are on same COL and 2 spaces apart
                elif cur_knot.position[1] == cur_knot.next.position[1] and abs(cur_knot.position[0] - cur_knot.next.position[0]) > 1:
                    # set cur_knot.next.position = prev_head and add coord to visited
                    cur_knot.next.position = cur_knot.prev_position
                
                # check if cur_knot.position and cur_knot.next.position are on dif ROW and COL and 2 spaces apart
                elif (cur_knot.position[0] != cur_knot.next.position[0] and cur_knot.position[1] != cur_knot.next.position[1]) and abs(cur_knot.position[0] - cur_knot.next.position[0]) > 1 or abs(cur_knot.position[1] - cur_knot.next.position[1]) > 1:
                    # set cur_knot.next.position = cur_knot.prev_position and add coord to visited
                    cur_knot.next.position = cur_knot.prev_position

            cur_knot = cur_knot.next

# Build the rope
my_rope = Rope()
for n in range(10):
    my_rope.add_knot(n)

# Update head position and move knots if needed
for move in move_list:
    direction = move[0]
    spaces = int(move[1])
    print(f"\nMoving head {spaces} {direction}")

    for space in range(spaces):
        my_rope.print_knots()
        my_rope.move_head(direction)


print(f"Tail visited: {len(set(my_rope.visited))}")
print("end")
