from copy import copy
visited = [(0,0)]
head = [0,0]
prev_head = [0,0]
tail = [0,0]
start = [0,0]
move_list = []

with open("data.txt", "r") as f:
    for line in f:
        move_list.append(line.rsplit())

def move_head(direction):
    if direction == "R":
        head[1] += 1
    
    elif direction == "L":
        head[1] -= 1
    
    elif direction == "U":
        head[0] += 1
    
    else: # "D"
        head[0] -= 1

for move in move_list:
    spaces = int(move[1])
    for x in range(spaces):
        prev_head = copy(head) # shallow copy of current head position to update tail if necessary
        move_head(move[0])
        # check if head and tail are on same ROW and 2 spaces apart
        if head[0] == tail[0] and abs(head[1] - tail[1]) > 1:
            # set tail = prev_head and add coord to visited
            tail = prev_head
            visited.append(tuple(tail))
            
        # check if head and tail are on same COL and 2 spaces apart
        elif head[1] == tail[1] and abs(head[0] - tail[0]) > 1:
            # set tail = prev_head and add coord to visited
            tail = prev_head
            visited.append(tuple(tail))
        
        # check if head and tail are on dif ROW and COL and 2 spaces apart
        elif (head[0] != tail[0] and head[1] != tail[1]) and abs(head[0] - tail[0]) > 1 or abs(head[1] - tail[1]) > 1:
            # set tail = prev_head and add coord to visited
            tail = prev_head
            visited.append(tuple(tail))
    
print(f"Tail visited {len(set(visited))} spots")

