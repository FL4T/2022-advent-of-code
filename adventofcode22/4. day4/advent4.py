# Day 4 Instructions - https://adventofcode.com/2022/day/4

part1_ans = 0
part2_ans = 0

# with open("4-test.txt", "r") as f:
with open("4-input.txt", "r") as f:
    for line in f:
        print(f"Working line: {line.rstrip()}")

        line = line.rstrip().split(",")

        grp_1 = line[0].split("-")
        grp_2 = line[1].split("-")

        if int(grp_1[0]) >= int(grp_2[0]) and int(grp_1[-1]) <= int(grp_2[-1]):
            print(f"Elf2 section, {grp_2}, contains Elf1 section, {grp_1}")
            part1_ans += 1
        
        elif int(grp_2[0]) >= int(grp_1[0]) and int(grp_2[-1]) <= int(grp_1[-1]):
            print(f"Elf1 section, {grp_1}, contains Elf2 section, {grp_2}")
            part1_ans += 1
        
        elif int(grp_2[0]) >= int(grp_1[0]) and int(grp_2[0]) <= int(grp_1[-1]):
            print(f"Elf2, {grp_2} OVERLAPS with ELF1, {grp_1}")
            part2_ans += 1

        elif int(grp_1[0]) >= int(grp_2[0]) and int(grp_1[0]) <= int(grp_2[-1]):
            print(f"Elf1, {grp_1} OVERLAPS with ELF2, {grp_2}")
            part2_ans += 1

        else:
            continue

print("The part 1 answer is", part1_ans)
print("The part 2 answer is", part2_ans + part1_ans)
        

