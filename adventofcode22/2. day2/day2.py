with open("test.txt", "r") as f:
    battles = [line.rstrip() for line in f]

''' PART 1
Enemy      Me
A = Rock   X = Rock  1pt
B = Papr   Y = Papr  2pt
C = Scis   Z = Scis  3pt
'''
WIN  = 6
DRAW = 3
LOSS = 0

total_score = 0

for battle in battles:
    foe_wpn = battle[0]
    my_wpn = battle[-1]

    if foe_wpn == "A" and my_wpn == "X":
        total_score += 1 + DRAW
    
    elif foe_wpn == "A" and my_wpn == "Y":
        total_score += 2 + WIN
    
    elif foe_wpn == "A" and my_wpn == "Z":
        total_score += 3 + LOSS
    
    elif foe_wpn == "B" and my_wpn == "X":
        total_score += 1 + LOSS
    
    elif foe_wpn == "B" and my_wpn == "Y":
        total_score += 2 + DRAW
    
    elif foe_wpn == "B" and my_wpn == "Z":
        total_score += 3 + WIN
        
    elif foe_wpn == "C" and my_wpn == "X":
        total_score += 1 + WIN
    
    elif foe_wpn == "C" and my_wpn == "Y":
        total_score += 2 + LOSS
    
    elif foe_wpn == "C" and my_wpn == "Z":
        total_score += 3 + DRAW
    else: 
        print("invalid scenario")

print(f"My Total Score: {total_score}")


    


