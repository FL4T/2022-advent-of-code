with open("data.txt", "r") as f:
    battles = [line.rstrip() for line in f]

WIN  = 6
DRAW = 3
LOSS = 0

ROCK = 1
PAPER = 2
SCISSOR = 3

total_score = 0

for battle in battles:
    foe_wpn = battle[0]
    outcome = battle[-1]

    # ROCK
    if foe_wpn == "A" and outcome == "X":
        total_score += SCISSOR + LOSS
    
    elif foe_wpn == "A" and outcome == "Y":
        total_score += ROCK + DRAW
    
    elif foe_wpn == "A" and outcome == "Z":
        total_score += PAPER + WIN
    
    # PAPER
    elif foe_wpn == "B" and outcome == "X":
        total_score += ROCK + LOSS
    
    elif foe_wpn == "B" and outcome == "Y":
        total_score += PAPER + DRAW
    
    elif foe_wpn == "B" and outcome == "Z":
        total_score += SCISSOR + WIN
    
    # SCISSOR
    elif foe_wpn == "C" and outcome == "X":
        total_score += PAPER + LOSS
    
    elif foe_wpn == "C" and outcome == "Y":
        total_score += SCISSOR + DRAW
    
    elif foe_wpn == "C" and outcome == "Z":
        total_score += ROCK + WIN
    else: 
        print("invalid scenario")

print(f"My Total Score: {total_score}")
