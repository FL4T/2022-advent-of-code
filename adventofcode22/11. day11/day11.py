from math import floor

with open("data.txt", "r") as f:
    data = f.readlines()
# print(data)

class Monkey:
    def __init__(self, id:int, items:list, op:str, op_num:int, test:int, tru:int, fls:int):
        self.id = id
        self.items = items
        self.operation = op
        self.op_num = op_num
        self.test = test
        self.true_throw = tru
        self.false_throw = fls
        self.inspect_count = 0
    
    # inspect item, them throw to next monkey
    def inspect(self):
        ret = []
        for item in self.items:
            self.inspect_count += 1
            if self.operation == "+":
                new = item + int(self.op_num)
            else: # self.operation == "*":
                if self.op_num == "old":
                    new = item * item
                else:
                    new = item * int(self.op_num)
            
            # worry_lvl = floor(new / 3)
            worry_lvl = new
            if worry_lvl % self.test == 0:
                pass_item = worry_lvl
                # print(f"Passing {pass_item} to Monkey {self.true_throw}")
                ret.append((pass_item, self.true_throw))
            else:
                pass_item = worry_lvl
                ret.append((pass_item, self.false_throw))
        self.items = []
        return ret

ROUNDS = 10000
monkey_lst = []
for line in data:
    line = line.rstrip(':\n').split()
    if "Monkey" in line:
        monkey_num = line[-1]
    
    elif "Starting" in line:
        item_lst = []
        for n in line:
            n = n.replace(',','')
            if n.isdigit():
                item_lst.append(int(n))
    
    elif "Operation:" in line:
        op = line[-2]
        op_num = line[-1]
    
    elif "Test:" in line:
        test_num = int(line[-1])
    
    elif "true:" in line:
        tru_test = int(line[-1])
    
    elif "false:" in line:
        fls_test = int(line[-1])
        
        monkey_lst.append(Monkey(monkey_num, item_lst, op, op_num, test_num, tru_test, fls_test))

# print(monkey_lst)

for round in range(ROUNDS):
    cur_monkey = 0
    for x in range(len(monkey_lst)):
        inspect_pass = monkey_lst[x].inspect()
        for throw in inspect_pass:
            monkey_lst[throw[1]].items.append(throw[0])

print(f"Monkey0 items inspected: {monkey_lst[0].inspect_count}")
print(f"Monkey1 items inspected: {monkey_lst[1].inspect_count}")
print(f"Monkey2 items inspected: {monkey_lst[2].inspect_count}")
print(f"Monkey3 items inspected: {monkey_lst[3].inspect_count}")

fin_lst = [monkey_lst[j].inspect_count for j in range(len(monkey_lst))]
fin_lst.sort()
print(f"Final Answer:{fin_lst[-1] * fin_lst[-2]}")
print(fin_lst)
pass
    