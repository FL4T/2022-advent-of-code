with open("data.txt", "r") as f:
    elf_lst = [line.rstrip() for line in f]

cal_lst = []
cal_count = 0
high_cals = 0
for x in elf_lst:
    if x != '':
        cal_count += int(x)
    
    else:
        if cal_count > high_cals:
            high_cals = cal_count
        cal_lst.append(cal_count)
        cal_count = 0

cal_lst.append(cal_count) # append final elf calorie to cal_list
print(f"Highest Cals: {high_cals}")

cal_lst.sort(reverse=True)
print(f"High 3 sum: {sum(cal_lst[:3])}")
