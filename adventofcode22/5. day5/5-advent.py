
stack1 = ['Z', 'P', 'M', 'H', 'R']
stack2 = ['P', 'C', 'J', 'B']
stack3 = ['S', 'N', 'H', 'G', 'L', 'C', 'D']
stack4 = ['F', 'T', 'M', 'D', 'Q', 'S', 'R', 'L']
stack5 = ['F', 'S', 'P', 'Q', 'B', 'T', 'Z', 'M']
stack6 = ['T', 'F', 'S', 'Z', 'B', 'G']
stack7 = ['N', 'R', 'V']
stack8 = ['P', 'G', 'L', 'T', 'D', 'V', 'C', 'M']
stack9 = ['W', 'Q', 'N', 'J', 'F', 'M', 'L']

def do_move(s:list, d:list):
    pop = s.pop()
    d.append(pop)

def move_part2(m:int, s:list, d:list):
    start_idx = len(s) - m
    tmp_lst = s[start_idx:]
    print(tmp_lst)
    for i in tmp_lst:
        d.append(i)
        s.pop()
    pass

with open("5-data.txt","r") as f:
    for line in f:
        line = line.rstrip() 

        # if "move" is in line, it contains work to be done
        if "move" in line:
            print(f"Working: {line}")
            
            # hold moves, src, dst
            line_nums = [] 

            # fill line_nums list
            split = line.split()
            for i in split:
                if i.isdigit():
                    line_nums.append(int(i))
            
            # do the moves contained in line_nums [# to move, source stack, destination stack]
            moves = line_nums[0]
            src = line_nums[1]
            dst = line_nums[2]
            
            # Determine source stack
            match src:
                case 1:
                    src = stack1
                case 2:
                    src = stack2
                case 3:
                    src = stack3
                case 4:
                    src = stack4
                case 5:
                    src = stack5
                case 6:
                    src = stack6
                case 7:
                    src = stack7
                case 8:
                    src = stack8
                case 9:
                    src = stack9
            
            # Determine destination stack
            match dst:
                case 1:
                    dst = stack1
                case 2:
                    dst = stack2
                case 3:
                    dst = stack3
                case 4:
                    dst = stack4
                case 5:
                    dst = stack5
                case 6:
                    dst = stack6
                case 7:
                    dst = stack7
                case 8:
                    dst = stack8
                case 9:
                    dst = stack9
        
            '''for part 1'''
            # for x in range(moves):
            #     do_move(src, dst)

            '''for part 2'''
            move_part2(moves, src, dst)

        else:
            print("No moves in current line, going to next line!")

print(f"The top of each stack is: {stack1[-1],stack2[-1],stack3[-1],stack4[-1],stack5[-1],stack6[-1],stack7[-1],stack8[-1],stack9[-1]}")