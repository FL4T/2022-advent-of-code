forest = []

with open("data.txt", "r") as f:
    for line in f:
        line = line.rstrip()
        # build the 2d list
        forest.append([int(n) for n in line])

ROW_SIZE = len(forest)
COL_SIZE = len(forest[0])
EDGE_TREES = (ROW_SIZE * 2 - 2) + (COL_SIZE * 2 - 2)
visible_trees = 0

def look_up(matrix:list, r, c):
    cur_tree = matrix[r][c]
    while r > 0:
        r -= 1
        if cur_tree > matrix[r][c] and r == 0:
            # print("Tree IS visible from top")
            return True
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from top")
            return False

def look_down(matrix:list, r, c):
    cur_tree = matrix[r][c]
    while r < COL_SIZE-1:
        r += 1
        if cur_tree > matrix[r][c] and r == COL_SIZE-1:
            # print("Tree IS visible from bottom")
            return True
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from bottom")
            return False

def look_left(matrix:list, r, c):
    cur_tree = matrix[r][c]
    while c > 0:
        c -= 1
        if cur_tree > matrix[r][c] and c == 0:
            return True
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from left")
            return False

def look_right(matrix:list, r, c):
    cur_tree = matrix[r][c]
    while c < ROW_SIZE-1:
        c += 1
        if cur_tree > matrix[r][c] and c == ROW_SIZE-1:
            return True
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from right")
            return False

for row in range(len(forest)):
    for col in range(len(forest)):
        # not an edge tree, look around
        if row != 0 and col != 0 and row != COL_SIZE-1 and col != ROW_SIZE-1:
            # look around
            # print(f"current position is forest[{row}][{col}]: {forest[row][col]}")
            if look_up(forest, row, col):
                # print("Tree IS visible from top")
                visible_trees += 1
            elif look_down(forest, row, col):
                # print("Tree IS visible from bottom")
                visible_trees += 1
            elif look_left(forest, row, col):
                # print("Tree IS visible from left")
                visible_trees += 1
            elif look_right(forest, row, col):
                # print("Tree IS visible from right")
                visible_trees += 1

print(f"Visible trees: {visible_trees + EDGE_TREES}")


