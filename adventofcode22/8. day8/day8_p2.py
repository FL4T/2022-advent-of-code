forest = []
with open("data.txt", "r") as f:
    for line in f:
        line = line.rstrip()
        # build the 2d list
        forest.append([int(n) for n in line])

ROW_SIZE = len(forest)
COL_SIZE = len(forest[0])
EDGE_TREES = (ROW_SIZE * 2 - 2) + (COL_SIZE * 2 - 2)
high_score = 0

def look_up(matrix:list, r, c):
    cur_tree = matrix[r][c]
    count = 0
    while r > 0:
        r -= 1
        if cur_tree > matrix[r][c]:
            count += 1
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from bottom")
            count += 1
            return count
    return count

def look_down(matrix:list, r, c):
    cur_tree = matrix[r][c]
    count = 0
    while r < COL_SIZE-1:
        r += 1
        if cur_tree > matrix[r][c]:
            count += 1
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from bottom")
            count += 1
            return count
    return count

def look_left(matrix:list, r, c):
    cur_tree = matrix[r][c]
    count = 0
    while c > 0:
        c -= 1
        if cur_tree > matrix[r][c]:
            count += 1
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from bottom")
            count += 1
            return count
    return count

def look_right(matrix:list, r, c):
    cur_tree = matrix[r][c]
    count = 0
    while c < ROW_SIZE-1:
        c += 1
        if cur_tree > matrix[r][c]:
            count += 1
        elif cur_tree <= matrix[r][c]:
            # print("Tree NOT visible from bottom")
            count += 1
            return count
    return count

for row in range(len(forest)):
    scene = [0, 0, 0, 0] #[up, down, left, right]
    for col in range(len(forest)):
        # not an edge tree, look around find scene_score
        if row != 0 and col != 0 and row != COL_SIZE-1 and col != ROW_SIZE-1:
            print(f"current position is forest[{row}][{col}]: {forest[row][col]}")
            
            scene[0] = look_up(forest, row, col)
            scene[1] = look_down(forest, row, col)
            scene[2] = look_left(forest, row, col)
            scene[3] = look_right(forest, row, col)

            scene_score = 1
            for x in scene:
                scene_score = scene_score * x
            
            if scene_score > high_score:
                high_score = scene_score
                print("cur high score: ", high_score)

print(f"Highest Score: {high_score}")


