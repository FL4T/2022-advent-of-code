import ast # look at src code sometime. 
           # found at https://stackoverflow.com/questions/1894269/how-to-convert-string-representation-of-list-to-a-list

with open("test.txt","r") as f:
    data = f.read().rsplit()
print(data)

good = 0
bad = 0

def check_order(lst1, lst2):
    
        if lst1[i] < lst2[i]:
            pair_done = True


# loop through pairs and convert the strings to lists using ast module
for pair in range(0,len(data),2):
    packet1 = ast.literal_eval(data[pair])
    packet2 = ast.literal_eval(data[pair + 1])
    print(f"\nWorking Packet Pair:\n{packet1}\n{packet2}")

    pair_done = False
    i = 0
    while not pair_done:
        # single list of same length
        if type(packet1[i]) == int and type(packet2[i]) == int:
            if packet1[i] < packet2[i]:
                good += 1
                pair_done = True
            i += 1
        
        # if first list contains int and next list contains list
        elif type(packet1[i]) == int and type(packet2[i] == list):
            if len(packet1[i]) == 1:
                pass
        

